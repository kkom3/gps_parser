import serial
import micropyGPS
import threading
import time
import pyproj
import smbus
from time import sleep
import time 
import numpy

gps = micropyGPS.MicropyGPS(9,'dd')
EPSG4612 = pyproj.Proj("+init=EPSG:4612")
EPSG2451 = pyproj.Proj("+init=EPSG:2451")
x_diff_gps = 0.0
y_diff_gps = 0.0



def rungps():
  s = serial.Serial('/dev/serial0',9600,timeout=10)
  s.readline()
  x_temp = 0.0
  y_temp = 0.0
  while True:
    sentence = s.readline().decode('utf-8')
    if sentence[0] != '$':
      continue
    for x in sentence:
      gps.update(x)
      lat = gps.latitude[0]
      lon = gps.longitude[0]

      if ( 139.5 < lon and lon <  140.5 ):
        if( 35 < lat and lat <  40):
          x,y = pyproj.transform(EPSG4612 ,EPSG2451 , lon,lat)
          global x_diff_gps
          global y_diff_gps
          x_diff_gps = x - x_temp
          y_diff_gps = y - y_temp
          x_temp = x
          y_temp = y
    sleep(0.1)

  #  with open('log_non_bar.txt') as f:
  #    line = f.readline()
  #    while line:
  #      print(line)
  #      line = f.readline()
  #      
  #      if line[0] != '$':
  #        continue
  #      for x in line:
  #        gps.update(x)

x_acc = 0.0
y_acc = 0.0
z_acc = 0.0
x_pos = 0.0
y_pos = 0.0
z_pos = 0.0
x_vel = 0.0
y_vel = 0.0
z_vel = 0.0
x_temp = 0
y_temp = 0
z_temp = 0
x_diff_imu = 0.0
y_diff_imu = 0.0





def runimu():
  global x_acc
  global y_acc
  global z_acc
  global x_pos
  global y_pos
  global z_pos
  global x_vel
  global y_vel
  global z_vel
  global x_temp 
  global y_temp 
  global z_temp 
  global x_diff_imu
  global y_diff_imu


  ACC_ADDRESS = 0x19
  GYR_ADDRESS = 0x69
  ACC_REGISTER_ADDRESS = 0x02
  GYR_REGISTER_ADDRESS = 0x02
  count = 0

#  x_acc_err = -0.041616
#  y_acc_err = -0.103457 + 0.04 + 0.006
#  z_acc_err = 0.957241
  x_acc_err = 0
  y_acc_err = 0 
  z_acc_err = 0

  xz_angle = 0.03659153846479307
  yz_angle = -0.0633990590766613

  rotate_xz = numpy.array([[numpy.cos(xz_angle),0,numpy.sin(xz_angle)], \
                          [0,1,0],                                      \
                          [-numpy.sin(xz_angle),0,numpy.cos(xz_angle)]])
  rotate_yz = numpy.array([[1,0,0],                                     \
                          [0,numpy.cos(yz_angle),-numpy.sin(yz_angle)], \
                          [0,numpy.sin(yz_angle),numpy.cos(yz_angle)]])

  i2c = smbus.SMBus(1)
  dt = 0.1
  print(x_vel)



  while True:

    ## 指定したレジスタアドレスから、2byte分取得する
    x_lsb = i2c.read_byte_data(ACC_ADDRESS, ACC_REGISTER_ADDRESS)
    x_msb = i2c.read_byte_data(ACC_ADDRESS, ACC_REGISTER_ADDRESS+1)
    y_lsb = i2c.read_byte_data(ACC_ADDRESS, ACC_REGISTER_ADDRESS+2)
    y_msb = i2c.read_byte_data(ACC_ADDRESS, ACC_REGISTER_ADDRESS+3)
    z_lsb = i2c.read_byte_data(ACC_ADDRESS, ACC_REGISTER_ADDRESS+4)
    z_msb = i2c.read_byte_data(ACC_ADDRESS, ACC_REGISTER_ADDRESS+5)

    i_lsb = i2c.read_byte_data(GYR_ADDRESS, GYR_REGISTER_ADDRESS)
    i_msb = i2c.read_byte_data(GYR_ADDRESS, GYR_REGISTER_ADDRESS+1)
    j_lsb = i2c.read_byte_data(GYR_ADDRESS, GYR_REGISTER_ADDRESS+2)
    j_msb = i2c.read_byte_data(GYR_ADDRESS, GYR_REGISTER_ADDRESS+3)
    k_lsb = i2c.read_byte_data(GYR_ADDRESS, GYR_REGISTER_ADDRESS+4)
    k_msb = i2c.read_byte_data(GYR_ADDRESS, GYR_REGISTER_ADDRESS+5)




    x_value = (x_msb * 16) + ((x_lsb & 0xF0) / 16)
    x_value = x_value if x_value < 2048 else x_value - 4096
    y_value = (y_msb * 16) + ((y_lsb & 0xF0) / 16)
    y_value = y_value if y_value < 2048 else y_value - 4096
    z_value = (z_msb * 16) + ((z_lsb & 0xF0) / 16)
    z_value = z_value if z_value < 2048 else z_value - 4096

    i_value = (i_msb * 256) + i_lsb
    i_value = i_value if i_value < 32767 else i_value - 65536
    j_value = (j_msb * 256) + j_lsb
    j_value = j_value if j_value < 32767 else j_value - 65536
    k_value = (j_msb * 256) + j_lsb
    k_value = k_value if k_value < 32767 else k_value - 65536

    i_value = i_value * 2000.0 /32767.0 
    j_value = j_value * 2000.0 /32767.0 
    k_value = k_value * 2000.0 /32767.0 


    x_acc = (x_value * 0.00098 - x_acc_err) * 10
    y_acc = (y_value * 0.00098 - y_acc_err) * 10
    z_acc = (z_value * 0.00098 - z_acc_err) * 10
    xyz_acc = numpy.array([x_acc,y_acc,z_acc])
    xyz_correct = numpy.dot(rotate_yz,numpy.dot(rotate_xz,xyz_acc))

    x_acc = xyz_correct[0]
    y_acc = xyz_correct[1]
    z_acc = xyz_correct[2]

    
    x_vel = x_acc * dt + x_vel 
    y_vel = y_acc * dt + y_vel 
    z_vel = z_acc * dt + z_vel 
#    x_pos = x_acc * dt * dt * 0.5 + x_vel * dt + x_pos
#    y_pos = y_acc * dt * dt * 0.5 + y_vel * dt + y_pos
#    z_pos = z_acc * dt * dt * 0.5 + z_vel * dt + z_pos
    x_pos = x_vel * dt + x_pos
    y_pos = y_vel * dt + y_pos
    z_pos = z_vel * dt + z_pos

    x_diff_imu = x_acc * dt * dt * 0.5 + x_vel * dt 
    y_diff_imu = y_acc * dt * dt * 0.5 + y_vel * dt

#        print("velocity")
#        print ("[%f, %f, %f]" % (x_vel, y_vel, z_vel))
#        print("position")
#        print ("[%f, %f, %f]" % (x_pos, y_pos, z_pos))
#        print("gyro")
#        print ("[%f, %f, %f]" % (i_value, j_value, k_value))
#        print ("------------")
    count += 1
    x_temp += x_acc
    y_temp += y_acc
    z_temp += z_acc
    sleep(0.1)



print('xy_acc imu: %2.8f, %2.8f, %2.8f' % (x_acc,y_acc,z_acc))
print('xy_vel imu: %2.8f, %2.8f, %2.8f' % (x_vel,y_vel,z_vel))
print('xy_pos imu: %2.8f, %2.8f, %2.8f' % (x_pos,y_pos,z_pos))


gpsthread = threading.Thread(target=rungps, args=())
gpsthread.daemon = True
gpsthread.start()

imuthread = threading.Thread(target=runimu, args=())
imuthread.daemon = True
imuthread.start()
now = time.time()
while True:
  if gps.clean_sentences > 20:
    lat = gps.latitude[0]
    lon = gps.longitude[0]
    if ( 139.5 < lon and lon <  140.5 ):
      if( 35 < lat and lat <  40):
        x,y = pyproj.transform(EPSG4612 ,EPSG2451 ,lon,lat)
        print('xy gps: %2.8f, %2.8f' % (x_diff_gps,y_diff_gps))
        print('xy_acc imu: %2.8f, %2.8f, %2.8f' % (x_acc,y_acc,z_acc))
        print('xy_vel imu: %2.8f, %2.8f, %2.8f' % (x_vel,y_vel,z_vel))
        print('xy_pos imu: %2.8f, %2.8f, %2.8f' % (x_pos,y_pos,z_pos))
        with open('/home/kkom/gps_parser/'+str(now)+'_gps.txt',mode='a') as f_gps:
          f_gps.write('%2.8f, %2.8f \n' % (x,y))
    with open('/home/kkom/gps_parser/'+str(now)+'_imu.txt',mode='a') as f_imu:
      f_imu.write('%2.8f, %2.8f, %2.8f \n' % (x_acc,y_acc,z_acc))

  sleep(1)







